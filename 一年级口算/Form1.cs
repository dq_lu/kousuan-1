﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 一年级口算
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int num = 0;

          

            while (num < 80)
            {
                bool rst = false;
                Random ra = new Random(unchecked((int)DateTime.Now.Ticks));
                int a = ra.Next(5, 20);
                int b = ra.Next(5, 20);
                int act = ra.Next(0, 2);

                if (a ==1|| b == 1)
                    continue;

               System.Threading. Thread.Sleep(100);

                if (act == 0)
                {
                    int result = a - b;
                    if (result > 0 && result < 20)
                    {
                        num++;
                        rst = true;
                    }
                }
                else
                {
                    int result = a + b;
                    if (result < 21 && result > 1)
                    {
                        num++;
                        rst = true;
                    }
                }
                if (rst == true)
                {
                    textBox1.Text += string.Format("{0}{1}{2}=\t", a, act == 0 ? "-" : "+", b);
                    if (num % 4 == 0)
                        textBox1.Text += "\r\n";
                }
            }

        }
    }
}
